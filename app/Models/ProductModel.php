<?php
namespace App\Models;

class ProductModel extends Model {
	protected static string $table = 'products';

	public function all(): \IdiormResultSet|array {
		// ->orderByDesc('id')
		return \ORM::forTable(self::$table)->where('is_activated', 1)->findMany();
	}

	public function getSome(int $perpage = 3, int $offset = 0): \IdiormResultSet|array {
		// ->orderByDesc('id')
		return \ORM::forTable(self::$table)->where('is_activated', 1)->limit($perpage)->offset($offset)->findMany();
	}

	public function getProduct(int $id): \ORM|bool {
		return \ORM::forTable(self::$table)->findOne($id);
	}

	public function create(array $product): bool {
		$newProduct = \ORM::forTable('products')->create();

		$newProduct->name       = $product['name'];
		$newProduct->created_at = date('Y-m-d H:i:s');
		$newProduct->save();

		return true;
	}

	public function count(): \ORM|int {
		return \ORM::forTable(self::$table)->where('is_activated', 1)->count();
	}

	public function activateAll(): \ORM {
		return \ORM::forTable(self::$table)->whereNull('is_activated')->findResultSet()
			->set('is_activated', 1)
			->save();
	}
}
