<?php
namespace App\Controllers;

use App\Models\ProductModel;
use App\Tools\Gc7;

class TestController extends Controller {
	public function test($i=1): string {
		$data = $i;
		
		return $this->template->render('pages/test.twig', ['data' => $data]);
	}

	public function count(): string {
		return $this->template->render('pages/test.twig');
	}

	public function increment(): string {
		return json_encode(['count' => ++$_SESSION['count']]);
	}

	public function name(): string {
		$data = json_decode(file_get_contents('php://input', 'r'));

		return json_encode(['data' => $data]);
	}
	
	public function nbTofs() {
		Gc7::aff(ROOT);
		
		// $dir = ROOT . '/src/assets/img';

		// $out = [];
		// foreach (glob($dir . '/*.jpg') as $filename) {
		// 	$p     = pathinfo($filename);
		// 	$out[] = $p['filename'];
		// }
		// return count($out);
	}

	public function pagination() {
		$currentPage = $_SESSION['page'] ?? 1;
		$perPage     = PERPAGE;
		$nbPdts      = (new ProductModel())->count();
		$nbPages     = ceil($nbPdts / $perPage);

		$offset = $perPage * ($currentPage - 1);

		$page = [
			'currentPage' => $currentPage,
			'perPage'     => $perPage,
			'nbPages'     => $nbPages,
		];

		$somePdts = (new ProductModel())->getSome($perPage, $offset);

		foreach ($somePdts as $k => $v) {
			$pdts[] = $v->id . ' (' . $k . '): ' . $v->name;
		}

		return ['page' => $page, 'pdts' => $pdts];
	}
}
