<?php

use Bluemmb\Faker\PicsumPhotosProvider;
use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class ProductsSeeder extends AbstractSeed {
	/**
	 * Run Method.
	 *
	 * Write your database seeder using this method.
	 *
	 * More information on writing seeders is available here:
	 * https://book.cakephp.org/phinx/0/en/seeding.html
	 */
	public function run(): void {
		define('DATEFORMAT', 'Y-m-d H:i:s');

		$data = [
			[
				'name'           => 'Marocain Rouge',
				'origin'         => '@Marocco',
				'is_activated'   => true,
				'image'          => '1.webp',
				'description'    => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Natus quas consequatur aut sequi facere maiores repellat at aspernatur atque necessitatibus tenetur voluptatum ipsa hic, nemo modi similique tempora, temporibus soluta, consequuntur iure? Asperiores corporis minus delectus laudantium accusamus dignissimos aliquam suscipit blanditiis distinctio placeat eveniet praesentium sequi consectetur, harum, dolores dolore iusto, nulla autem id mollitia numquam. Modi minus vitae explicabo neque. Illum libero eos odit modi iure, sunt accusamus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris. @bulmaio. #css #responsive.',
				'delivery_delay' => 21,
				'price'          => 25,
				'created_at'     => date(DATEFORMAT),
			], [
				'name'           => 'Cannabis',
				'origin'         => '@Asian',
				'is_activated'   => true,
				'image'          => '2.webp',
				'description'    => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid, incidunt vel! Consequuntur, unde dolorum est voluptatibus assumenda sunt tempore perferendis sequi officia ut officiis quis deserunt quisquam eius. Voluptatem, odit. Form cannabis Sativa.',
				'delivery_delay' => 7,
				'price'          => 20,
				'created_at'     => date(DATEFORMAT),
			], [
				'name'           => 'CBD',
				'origin'         => '@France',
				'is_activated'   => true,
				'image'          => '3.webp',
				'description'    => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni qui odio, quis exercitationem voluptas quidem voluptatem delectus sint obcaecati deserunt, nesciunt eveniet tenetur dolorum ab quibusdam natus adipisci assumenda. Distinctio illum veniam ea error id odit neque harum voluptate nulla voluptatum? Maxime eaque nulla quidem doloremque, dolorem magni? Obcaecati, exercitationem.',
				'delivery_delay' => 3,
				'price'          => 50,
				'created_at'     => date(DATEFORMAT),
			],
		];

		$nbPdts = 1e2;

		$faker = Factory::create();

		for ($i = 4; $i < $nbPdts+1; ++$i) {
			$names[$i] = $faker->unique()->words($faker->numberBetween(1, 2), true);
		}

		for ($i = 4; $i < $nbPdts+1; ++$i) {
			$faker->addProvider(new PicsumPhotosProvider($faker));

			// 2do Stocker imgs

			$data[] = [
				'name'           => ucwords($names[$i]),
				'origin'         => '@' . $faker->country(),
				'is_activated'   => $faker->optional($weight = .9)->randomElement([1]),
				'image'          => $i . '.jpg',
				'description'    => $faker->paragraph(2),
				'delivery_delay' => $faker->randomElement([3, 5, 7, 14, 21, 30]),
				'price'          => $faker->numberBetween(1, 99),
				'created_at'     => date('Y-m-d H:i:s'),
			];
		}

		$products = $this->table('products');
		$products->truncate();
		$products->insert($data)
			->saveData();
	}
}
