export default async () => {
  let counter = document.getElementById('goCount');

  counter.addEventListener('click', async () => {
    let response = await fetch('/increment');
    let data = await response.json();

    const textElement = document.getElementById('test');
    textElement.innerText = data.count;
  });
}
